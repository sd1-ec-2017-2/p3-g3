const setupRegistration = require('./irc-amqp/registration');
const setupIrcCommands = require('./irc-amqp/irc-commands');

let proxies = {};
let amqp = {
	lib: require('amqplib/callback_api'),
	connection: null,
	channel: null,
	send: function(queue, message, sendOptions) {
		this.channel.assertQueue(queue, { durable: false });

		if (typeof message !== 'string')
			message = JSON.stringify(message);

		this.channel.sendToQueue(queue, Buffer.from(message), sendOptions);
	},
	sendIrcEvent: function(queue, event, data) {
		this.send(queue, {
	    type: 'irc event',
	    event: event,
	    data: data
	  });
	}
};

amqp.lib.connect('amqp://localhost', function(err, conn) {
	if (err)
		return console.error(err);

	amqp.connection = conn;
  conn.createChannel(function(err, ch) {
		if (err)
			return console.error(err);

  	amqp.channel = ch;
		setupRegistration(proxies, amqp);
		setupIrcCommands(proxies, amqp);
		// teste();
  });
});

function teste() {
  let queue = 'registration';
	return amqp.channel.deleteQueue(queue, function(err, ok) {
		console.log('deleted');
	});
  amqp.channel.assertQueue(queue, { durable: false });

  amqp.channel.consume(queue, function(msg) {
    console.log(`[${new Date()}] Received on consumer 1: `);
    console.log(JSON.parse(msg.content.toString()));
  }, { noAck: true });

  amqp.channel.consume(queue, function(msg) {
    console.log(`[${new Date()}] Received on consumer 2: `);
    console.log(JSON.parse(msg.content.toString()));
  }, { noAck: true });
}
