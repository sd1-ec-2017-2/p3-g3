module.exports = function(proxies, amqp) {
  amqp.channel.assertQueue('irc', { durable: false });
  amqp.channel.consume('irc', function(msg) {
    let message = JSON.parse(msg.content.toString()),
      id = message.id,
      proxy = proxies[id];

    console.log('Message Arrived: ' + msg.content.toString());

    // Evita crash caso o id não exista
    if (!proxy) return;

    if (comandos.hasOwnProperty(message.command))
      comandos[message.command](proxy, message.data);
  }, { noAck: true });
}

let comandos = {};

comandos.disconnect = function(proxy) {
  if (!proxy.manterAtivo)
    proxy.close();
};

comandos.quit = function(proxy, data) {
  proxy.close(data);
};

comandos.message = function(proxy, data) {
  proxy.irc.say(data.to, data.message);
};

comandos.join = function(proxy, data) {
  proxy.irc.join(data);
};

comandos.part = function(proxy, data) {
  proxy.irc.part(data.canal, data.mensagem);
};

comandos.whois = function(proxy, data) {
    proxy.irc.whois(data);
};

comandos.names = function(proxy, data) {
  proxy.irc.send('names', data.channel);
};

comandos.nick = function(proxy, data) {
  proxy.irc.send('nick', data);
};

comandos.action = function(proxy, data) {
  proxy.irc.action(data.target, data.message);
};

comandos.motd = function(proxy) {
  proxy.irc.send('motd');
};

comandos.mode = function(proxy, data) {
  proxy.irc.send('mode', ...data);
};

comandos.list = function(proxy) {
  proxy.irc.list();
};

comandos.notice = function(proxy, data) {
    proxy.irc.notice(data.to, data.message);
};
