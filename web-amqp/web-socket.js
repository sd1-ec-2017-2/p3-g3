// Tratamento dos comandos enviados pelo usuario

module.exports = function(proxy) {
  let socket = proxy.socket;
  let amqp = proxy.amqp;

  socket.on('message', function(data) {
    amqp.send('irc', {
      id: proxy.id,
      command: 'message',
      data: data
    });
  });

  socket.on('join', function(canais) {
    amqp.send('irc', {
      id: proxy.id,
      command: 'join',
      data: canais
    });
  });

  socket.on('part', function(data) {
    amqp.send('irc', {
      id: proxy.id,
      command: 'part',
      data: data
    });
  });

  socket.on('quit', function(mensagem) {
    proxy.quit = true;
    amqp.send('irc', {
      id: proxy.id,
      command: 'quit',
      data: mensagem
    });
  });

  socket.on('whois', function(data) {
    amqp.send('irc', {
      id: proxy.id,
        command: 'whois',
        data: data
      });
  });

  socket.on('names', function(canal) {
    amqp.send('irc', {
      id: proxy.id,
      command: 'names',
      data: canal
    });
  });

  socket.on('action', function(data) {
    amqp.send('irc', {
      id: proxy.id,
      command: 'action',
      data: data
    });
  });

  socket.on('nick', function(data) {
    amqp.send('irc', {
      id: proxy.id,
      command: 'nick',
      data: data
    });
  });

  socket.on('motd', function() {
    amqp.send('irc', {
      id: proxy.id,
      command: 'motd'
    });
  });

  socket.on('mode', function(data) {
    amqp.send('irc', {
      id: proxy.id,
      command: 'mode',
      data: data
    });
  });

  socket.on('list', function() {
    amqp.send('irc', {
      id: proxy.id,
      command: 'list'
    });
  });

  socket.on('notice', function(data) {
    amqp.send('irc', {
      id: proxy.id,
      command: 'notice',
      data: data
    });
  });
}
