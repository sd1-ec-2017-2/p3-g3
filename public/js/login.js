// Realiza a validação dos dados fornecidos

$(function() {
  $('#login-form').submit(function() {
    var nome = $('#nome').val();
    var canal = $('#canal').val();
    var servidor = $('#servidor').val();

    if (!(nome && canal && servidor)) {
      alert('Preencha todos os campos!');
      return false;
    }

    return true; // Envia para o servidor
  });

  $('#login-id-form').submit(function() {
    if (!$('#id').val()) {
      alert('Preencha todos os campos!');
      return false;
    }

    return true; // Envia para o servidor
  });

  $('#id-login').click(function() {
    window.location = '/login/id';
  });
});

function mudarServidor(server, canal) {
  $('#servidor').val(server);

  if (!$('#canal').val())
    $('#canal').val(canal);
}
